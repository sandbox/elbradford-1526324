<?php 
/**
 * @file
 *
 * Processes the batch of Mass Apply Filter
 *
 */

function mass_apply_filter_batch_node($options1, $options2, $options3, $options4, $node_id, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_node'] = 0;
    $context['sandbox']['max'] = db_result(db_query("SELECT COUNT(DISTINCT nid) FROM {node} WHERE type='%s'", $options1));
  }

  //Load the node
  $node = node_load($node_id, NULL, TRUE);
  //Run the body through the specified filter
  $body = check_markup($node->body, $options2, FALSE);
  //Run the teaser through the specified filter
  $teaser = node_teaser($node->body, $options2);
  $format = $node->format;

  //Update the format property if checked
  if ($options3 = 1)
    $format = $options4;

  //Update the node values
  $node->body = $body;
  $node->teaser = $teaser;
  $node->format = $format;

  // Store some result for post-processing in the finished callback.
  $context['results'][] = check_plain($node->title);

  //Save the node to the database
  node_save($node);

  // Update our progress information.
  $context['sandbox']['progress']++;
  $context['sandbox']['current_node'] = $node_id;
  $context['message'] = t('%d Items: Now processing %node', $context['sandbox']['max'], array('%node' => $node->title));

  $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
}

function mass_apply_filter_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = count($results) .' processed.';
    $message .= theme('item_list', $results);
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
  }
  drupal_set_message($message);
}